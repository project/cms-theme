<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>

<table border="0" cellpadding="0" cellspacing="0" id="header">
 	<tr><td align="center"  class="wh1" colspan="2"  > </td>
</tr>
<?php if ($site_slogan) { ?>
<tr>
		<td height="21" align="center"  class="wh1" colspan="2"  ><div class='site-slogan'><?php print $site_slogan ?></div></td>
</tr>
<?php } ?>		
  <tr>
    <td id="logo" align="left" class="logo_td"><?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" hspace="5" /></a> <?php } ?>
 
<?php if ($site_name) { ?><h2 class='site-name'><a href="<?php print $base_path ?>"  title="<?php print t('Home') ?>"><?php print $site_name ?></a></h2><?php } ?>
     
    </td>
    
  </tr>
  <tr>
		<td class="wh1" height="21" align="center" colspan="2" id="menu">
		<?php if (isset($secondary_links)) { ?><?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist')) ?><?php } ?>
      <?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?><?php } ?>
		
		</td>
</tr>	
  <tr>
    <td colspan="2"><div><?php print $header ?></div></td>
  </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" id="content">
  <tr>
    <?php if ($sidebar_left) { ?><td id="sidebar-left">
      <?php print $sidebar_left ?>
    </td><?php } ?>
    <td valign="top">
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <div id="main">
        <?php print $breadcrumb ?>
        <?php if($title) { ?><h1 class="title"><?php print $title ?></h1><? }?>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
		 <?php print '<div class="space"> </div>'; ?>
        <?php print '<div id="rss12">'.$feed_icons.'</div>'; ?>
      </div>
    </td>
    <?php if ($sidebar_right) { ?><td id="sidebar-right">
      <?php print $sidebar_right ?>
    </td><?php } ?>
  </tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
		<td align="center"  class="wh1" colspan="2"><?php print $footer_message ?></td></tr>
</table>
<?php print $closure ?>
</body>
</html>